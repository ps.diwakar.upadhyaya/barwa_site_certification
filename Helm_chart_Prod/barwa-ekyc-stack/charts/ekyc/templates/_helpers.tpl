{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ekyc.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "ekyc.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create short name
*/}}
{{- define "ekyc.shortname" -}}
{{- "ekyc" | trunc 4 -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ekyc.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "ekyc.labels" -}}
app.kubernetes.io/name: {{ include "ekyc.name" . }}
helm.sh/chart: {{ include "ekyc.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app-name: ekyc-api
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{- define "ekyc.api_database_username" -}}
{{- if .Values.database.username -}}
{{- .Values.database.username -}}
{{- else -}}
{{- printf "%s_%s" (.Release.Name | replace "-" "" | trunc 20) "api" -}}
{{- end -}}
{{- end -}}

{{- define "ekyc.api_database_password" -}}
{{- if .Values.database.password -}}
{{- .Values.database.password -}}
{{- else -}}
{{- printf "%s_%s" (.Release.Name | replace "-" "" | trunc 20) "api" -}}
{{- end -}}
{{- end -}}

{{/*
Default version
*/}}
{{- define "ekyc.tag" -}}
{{- if .Values.image.tag -}}
{{- .Values.image.tag -}}
{{- else -}}
{{- if .Values.global -}}
{{- .Values.global.psci_version | default "latest" }}
{{- else -}}
{{- "latest" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Default Repo
*/}}
{{- define "ekyc.repo" -}}
{{- if .Values.image.repo -}}
{{- .Values.image.repo -}}
{{- else -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_url | default "harbor.progressoft.io" }}
{{- else -}}
{{- "harbor.progressoft.io" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Domain
*/}}
{{- define "devopsDomain" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_devops_domain | default "progressoft.dev" }}
{{- else -}}
{{- "progressoft.dev" -}}
{{- end -}}
{{- end -}}

{{/*
Registry
*/}}
{{- define "registryHost" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_url | default "localhost:32000" }}
{{- else -}}
{{- "localhost:32000" -}}
{{- end -}}
{{- end -}}

{{- define "registryUser" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_username | default "" }}
{{- else -}}
{{- "" -}}
{{- end -}}
{{- end -}}

{{- define "registryPass" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_password | default "" }}
{{- else -}}
{{- "" -}}
{{- end -}}
{{- end -}}

{{- define "keycloak.service" -}}
{{- $name := printf "%s-%s" .Release.Name "keycloak" | trunc 20 | trimSuffix "-" -}}
{{- printf "%s-http" $name -}}
{{- end -}}
