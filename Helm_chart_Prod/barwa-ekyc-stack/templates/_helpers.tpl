{{/* vim: set filetype=mustache: */}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "job.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "job.labels" -}}
app.kubernetes.io/name: {{ include "ekyc.name" . }}
helm.sh/chart: {{ include "ekyc.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}


{{- define "appdbinit.api_database_username" -}}
{{- printf "%s_%s" (.Release.Name | replace "-" "" | trunc 20) "api" -}}
{{- end -}}

{{- define "appdbinit.api_database_password" -}}
{{- printf "%s_%s" (.Release.Name | replace "-" "" | trunc 20) "api" -}}
{{- end -}}


{{/*
Domain
*/}}
{{- define "devopsDomain" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_devops_domain | default "progressoft.dev" }}
{{- else -}}
{{- "progressoft.dev" -}}
{{- end -}}
{{- end -}}

{{/*
Registry
*/}}
{{- define "registryHost" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_url | default "localhost:32000" }}
{{- else -}}
{{- "localhost:32000" -}}
{{- end -}}
{{- end -}}

{{- define "registryUser" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_username | default "" }}
{{- else -}}
{{- "" -}}
{{- end -}}
{{- end -}}

{{- define "registryPass" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_password | default "" }}
{{- else -}}
{{- "" -}}
{{- end -}}
{{- end -}}
