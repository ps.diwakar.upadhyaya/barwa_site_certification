#!/bin/bash

cp /usr/local/tomcat/webapps/ROOT/WEB-INF/properties/startup.properties-bkp /usr/local/tomcat/webapps/ROOT/WEB-INF/properties/startup.properties
cp /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/core/system-generic.properties-bkp /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/core/system-generic.properties

if [ "$liquibase_enabled" = true ] ; then
    echo "|-------------------------------------------------------|"
    echo "|       Executing database migration scripts            |"
    echo "|-------------------------------------------------------|"
    cat /liquibase/scripts/liquibase-master.xml | grep include
    if [ "$db_engine" = "postgres" ]; then
	echo "###liquibase-scripts ###"
        cd /liquibase; ./liquibase --driver=$db_driver --classpath=/usr/local/tomcat/webapps/ROOT/WEB-INF/lib/postgresql-42.2.1.jar --url=$db_url --username=$db_user --password=$db_password --changeLogFile=./scripts/liquibase-master.xml update
	echo "###liquibase-common ###"
	cd /liquibase; ./liquibase --driver=$db_driver --classpath=/usr/local/tomcat/webapps/ROOT/WEB-INF/lib/postgresql-42.2.1.jar --url=$db_url --username=$db_user --password=$db_password --changeLogFile=./common/liquibase-master.xml update
    if [ "$?" != "0" ]; then echo "Executing migration scripts failed"; exit 1; fi;
    echo "|-------------------------------------------------------|"
    echo "|       Done executing migration scripts                |"
    echo "|-------------------------------------------------------|"
  fi  
fi

sed -i 's/SYSTEM/BARWA/g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/app/camel/routes/MPClearIntegration/*
sed -i 's/tenantId=.*"/tenantId=BARWA"/g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/app/camel/routes/MPClearIntegration/mpayAutoLogin.xml
sed -i 's/default.tenant=SYSTEM/default.tenant=BARWA/g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/core/system-generic.properties
sed -i 's|7777/BARWA|7070|g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/app/camel/app-camel-settings.properties

cd /usr/local/tomcat/bin || exit
catalina.sh jpda run

