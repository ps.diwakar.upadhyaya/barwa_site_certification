#!/bin/bash

RELEASE_VERSION=v2.3.0
REGISTRY_URL=10.0.111.154:5000

#ARTEMIS_RELEASE_VERSION=2.16.0-alpine

echo "####Loading docker images####"
for tarImage in *.tar
do podman load < $tarImage
done
echo "####Done####"

echo "####Tagging docker images####"
#podman tag localhost/vromero/activemq-artemis:$ARTEMIS_RELEASE_VERSION $REGISTRY_URL/activemq-artemis:$ARTEMIS_RELEASE_VERSION
podman tag harbor.progressoft.io/quantum/barwa-barwa-ui:$RELEASE_VERSION $REGISTRY_URL/barwa-ui:$RELEASE_VERSION
podman tag harbor.progressoft.io/quantum/barwa-barwa-apis:$RELEASE_VERSION $REGISTRY_URL/barwa-apis:$RELEASE_VERSION
echo "####Done####"

echo "####Pushing docker images####"
#podman push $REGISTRY_URL/activemq-artemis:$ARTEMIS_RELEASE_VERSION
podman push $REGISTRY_URL/barwa-ui:$RELEASE_VERSION
podman push $REGISTRY_URL/barwa-apis:$RELEASE_VERSION
echo "####Done####"
