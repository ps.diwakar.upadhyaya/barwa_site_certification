```c#
##Database Production:##

IP: 10.0.100.244
DB name: mpayprd
Port: 5432

Username: mpay
Password:mp@y#21     

Username: ekyc
Password: eky@#21
```

***Database import/export********************
```c#
Export Postgres DB dump:
pg_dump -U db_user db_name > dump_name.sql

pg_dump -U mpay postgres > dump_name.sql

Import Postgres DB dump:
psql -f dbbarwa-mpay.sql  -p 5432 -U db_user -d db_name -v ON_ERROR_STOP=1

psql -f dbbarwa-mpay.sql  -p 5432 -U mpay -d postgres -v ON_ERROR_STOP=1

Create Postgres DB user:
CREATE USER mpay WITH PASSWORD 'mpay';
ALTER USER mpay WITH SUPERUSER;
ALTER USER mpay SET search_path to 'mpay';

CREATE Postgres DB_USER FOR ekyc:
CREATE DATABASE ekyc;
create user ekyc with password 'ekyc';
grant all privileges on database ekyc to ekyc;


To Drop all Postgres DB objects:
postgres=# DROP OWNED BY mpay;
postgres=# DROP user  mpay ;

DUKH Mpay DB main param:
update mpay.mpay_sysconfigs set configvalue='http://<api-service-name>:8080/processMessage?token=y' where configkey='Spring Service Url';
update mpay.mpay_sysconfigs set configvalue='http://<api-service-name>:8080/processMessage?token=y' where configkey='Spring Reverse Url';
update mpay.mpay_sysconfigs set configvalue='DUKHTELLER1@DUKH' where configkey='MPCLEAR TELLER ALIAS';

update mpay.mpay_sysconfigs set configvalue='01-01-900-52-61-2907035012' where configkey='Bank Pool Account';
update mpay.mpay_sysconfigs set configvalue='01-01-900-19-01-1003000020' where configkey='Bank QCB Clearing Account';
update mpay.mpay_sysconfigs set configvalue='01-01-900-52-61-4005010011' where configkey='Credit Bank ONUS Fee Account';
update mpay.mpay_sysconfigs set configvalue='01-01-900-52-61-4005010012' where configkey='Credit Bank OFFUS Fee Account';		
```